"""

    Lamptop development utilities
    Copyright (C) 2011  Colby T. Sollars

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
"""

from config import *
import argparse
import sys
import os
from lamptop_action import *

class lamptopApp:
    config = None # Config object
    def __init__(self):
        self.config = config()
        help_desc = "Lamptop command list:\n"
        help_desc+= "\tcreate_virtualhost\n"
        help_desc+= "\tdatabase\n"
        help_desc+= "\tcreate_project\n"
        help_desc+= "\thelp\n"
        help_desc+= "enter 'lamptop [command] help' to see the usage of each command.'"
        self.arg_parser = argparse.ArgumentParser(
            prog = "lamptop",
            description = help_desc,
            add_help = False
        )
        self.arg_parser.add_argument(
            "command",
            nargs="?",
            default="help"
        )
        self.arg_parser.add_argument(
            "subcommand",
            nargs="?"
        )
        self.arg_parser.add_argument(
            "options",
            nargs="*"
        )
        self.arg_parser.add_argument(
            "--force",
            action="store_true"
        )
        self.arg_parser.add_argument(
        	"--sql_file",
        	action="store"
        )
        
    def run(self):
        args = sys.argv
        args.pop(0)
        args_parsed = self.arg_parser.parse_args(args)
        
        if self.config.show_legal:
            self.show_legal()
        
        if args_parsed.command == "help":
            self.print_help()
            return
        elif args_parsed.command == "create_project":
            action = lamptopCliAction_createProject(self)
        elif args_parsed.command == "create_virtualhost":
            action = lamptopCliAction_createVirtualhost(self)
        elif args_parsed.command == "database":
            action = lamptopCliAction_database(self)
        elif args_parsed.command == "show_config":
            self.print_config()
            return
        else:
            self.print_help()
            return
        
        self.sanity_test()
        action.subcommand = args_parsed.subcommand
        action.options = args_parsed.options
        action.force = args_parsed.force
        action.sql_file = args_parsed.sql_file
        action.run()
    
    def print_config(self):
        print "\nLamptop Configuration:"
        print "  Default root directory:\t%s" % self.config.default_root_directory
        print "  Document root UID:\t\t%i" % self.config.web_user
        print "  Document root GID:\t\t%i" % self.config.web_group
        print "  Host file path:\t\t%s" % self.config.host_file_path
        print "  Localhost IP:\t\t\t%s\n" % self.config.localhost_ip
    
    def print_help(self):
        print "usage: lamptop [--force] [command] [subcommand] [options [options ...]]\n"
        print "Lamptop command list:"
        print "\tcreate_virtualhost"
        print "\tdatabase"
        print "\tcreate_project"
        print "\tcreate_help"
        print "\nenter 'lamptop [command] help' to see the usage of each command.'\n"

        print "optional arguments:"
        print "  --force\tWon't ask if you're sure."
    
    def show_legal(self):
        print "\nLamptop Copyright (C) 2011  Colby T. Sollars"
        print "This program comes with ABSOLUTELY NO WARRANTY\n\n"
        
    def sanity_test(self):
        if not os.getuid() == 0:
            print "This application must be run as the user 'root'. Quitting."
            sys.exit()
        if not os.path.exists(self.config.host_file_path):
            print "Cannot find host file. Quitting."
            sys.exit()
