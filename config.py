"""

    Lamptop development utilities
    Copyright (C) 2011  Colby T. Sollars

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
"""

import os
import os.path

class config:
    def __init__(self):
        self.default_root_directory = "/var/www/"
        self.web_user = 0 # Currently Set to root. CHANGE THIS
        self.web_group = 0 # Currently Set to root. CHANGE THIS
        self.host_file_path = "/etc/hosts"
        self.localhost_ip = "127.0.0.1"
        self.show_legal = True
        self.vhost_template_content = """<VirtualHost *:80>
	ServerName {servername}

	DocumentRoot {document_root}
	<Directory {document_root}>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>

	ErrorLog ${APACHE_LOG_DIR}/error.log

	# Possible values include: debug, info, notice, warn, error, crit,
	# alert, emerg.
	LogLevel warn

	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>"""
        
        # Check for /etc/lamptop/lamptop.cfg configuration
        if os.path.exists("/etc/lamptop/lamptop.conf"):
            self._load_cfg("/etc/lamptop/lamptop.conf")
        
        #load /etc/lamptop/vhost_template if it exists
        if os.path.exists("/etc/lamptop/vhost_template"):
            fobj = open("/etc/lamptop/vhost_template")
            self.vhost_template_content = fobj.read()
            fobj.close()
    
    def _load_cfg(self, filename):
        fobj = open(filename, "r")
        for line in fobj.readlines():
            if line[0] in ["#", ";"]:
                continue
            if line.find("=") == -1:
                continue
            line = line.replace("\n", "")
            (key, val) = line.split("=")
            key = key.replace(" ", "")
            val = val.replace(" ", "")
            if key == "default_root_directory":
                self.default_root_directory = val
            if key == "web_user":
                self.web_user = int(val)
            if key == "web_group":
                self.web_group = int(val)
            if key == "host_file_path":
                self.host_file_path = val
            if key == "localhost_ip":
                self.localhost_ip = val
            if key == "default_root_directory":
                self.show_legal = val[0].upper() == "T"
            
            fobj.close()
