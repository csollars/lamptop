"""

    Lamptop development utilities
    Copyright (C) 2011  Colby T. Sollars

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
"""


class lamptopCliAction_base:
    subcommand = ""
    options = None # array
    force = False
    sql_file = None
    app = None # App obj
    def __init__(self, app):
        self.app = app
        self.options = []
        
    def run(self):
        if self.subcommand == "help":
            self.show_help()
            return
        errors = self.do_action(self.subcommand, self.options)
        if len(errors):
            self.failure_message(errors)
            return
        self.success_message()
    
    def show_help(self):
        pass
    
    def do_action(self, subcommand="", options=[]):
        pass
    
    def failure_message(self, errors):
        pass
    
    def success_message(self):
        pass
    
    def _get_validated_input(self, query_msg, error_msg, validator, default_value="", is_passwd=False):
        input_func = raw_input
        if is_passwd:
            input_func = getpass.unix_getpass
        value = input_func(query_msg)
        if not value:
            value = default_value
        error_list = validator(value)
        error = False
        if len(error_list):
            error = True
        while error:
            print error_msg
            for err in error_list:
                print "    " + err
            value = input_func(query_msg)
            if not value:
                value = default_value
            error_list = validator(value)
            if len(error_list):
                error = True
            else:
                error = False
        return value
