"""

    Lamptop development utilities
    Copyright (C) 2011  Colby T. Sollars

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
"""

from base import lamptopCliAction_base
from create_virtualhost import lamptopCliAction_createVirtualhost
from  database import lamptopCliAction_database

class lamptopCliAction_createProject(lamptopCliAction_base):
    def __init__(self, app):
        lamptopCliAction_base.__init__(self, app)
        self.vhost_action = lamptopCliAction_createVirtualhost(self.app)
        self.db_action = lamptopCliAction_database(self.app)
        
    def do_action(self, subcommand="", options=[]):
        lamptopCliAction_base.do_action(self)
        vhost_errors = self.vhost_action.do_action()
        if len(vhost_errors):
            return vhost_errors
        
        do_db = raw_input("Would you like to set up a MySql database? [y]: ")
        if do_db in ["n", "N"]:
            return []
        
        return self.db_action.do_action("create")
    
    def failure_message(self, errors):
        print "Failed to set up new project for the following reasons:"
        for err in errors:
            print "    " + err
    
    def success_message(self):
        print "\nNew project successfully set up. To use your new virtualhost please restart apache."
        if self.db_action.db_name:
            print "You can log into your new MySql database with the following command:"
            print "mysql %s -u %s -p%s" % (self.db_action.db_name, self.db_action.db_user, self.db_action.db_pass)

    def show_help(self):
        print "usage: lamptop create_project\n"
        print "Creates a new Apache virtualhost and optionally a MySql database."
        print "It will ask you interactively for any needed info\n"
